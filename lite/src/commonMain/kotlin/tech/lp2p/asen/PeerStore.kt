package tech.lp2p.asen


interface PeerStore {
    suspend fun peeraddrs(limit: Int): List<Peeraddr>

    suspend fun storePeeraddr(peeraddr: Peeraddr)
}
package tech.lp2p.asen


// hash is always (32 bit) and it is a Ed25519 public key
data class PeerId(val hash: ByteArray) {

    override fun hashCode(): Int {
        return hash.contentHashCode() // ok, checked, maybe opt
    }

    fun toBase58(): String {
        return encode58(multihash(this))
    }

    init {
        require(hash.size == HASH_LENGTH) { "hash size must be 32" }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as PeerId

        return hash.contentEquals(other.hash)
    }

}

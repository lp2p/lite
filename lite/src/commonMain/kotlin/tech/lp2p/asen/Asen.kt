package tech.lp2p.asen

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import kotlinx.io.readUShort


const val HASH_LENGTH: Int = 32

const val IP4: Int = 4
const val IP6: Int = 41
const val UDP: Int = 273


private val Ed25519_ID_PREFIX = byteArrayOf(0, 36, 8, 1, 18, 32)
private val Ed25519_PREFIX = byteArrayOf(8, 1, 18, 32)


private const val QUICV1: Int = 461

private fun size(code: Int): Int {
    return when (code) {
        IP4 -> 32
        IP6 -> 128
        UDP -> 16
        QUICV1 -> 0
        else -> -1
    }
}

private fun readUnsignedVariant(buffer: Buffer): Int {
    var result = 0
    var cur: Int
    var count = 0
    do {
        cur = buffer.readByte().toInt() and 0xff
        result = result or ((cur and 0x7f) shl (count * 7))
        count++
    } while (((cur and 0x80) == 0x80) && count < 5)
    check((cur and 0x80) != 0x80) { "invalid unsigned variant sequence" }
    return result
}

private fun sizeForAddress(code: Int, cis: Buffer): Int {
    val size = size(code)
    if (size > 0) return size / 8
    if (size == 0) return 0
    return readUnsignedVariant(cis)
}

private fun readPart(code: Int, cis: Buffer): Any? {
    try {
        val sizeForAddress = sizeForAddress(code, cis)
        when (code) {
            IP4, IP6 -> {
                return cis.readByteArray(sizeForAddress)
            }

            UDP -> {
                //val a = cis.readByte().toInt() and 0xFF
                //val b = cis.readByte().toInt() and 0xFF
                return cis.readUShort()
            }
        }
    } catch (_: Throwable) {
    }
    return null
}

fun identifyPeerId(peerId: PeerId): ByteArray {
    return concat(Ed25519_PREFIX, peerId.hash)
}

fun identifyRaw(raw: ByteArray): PeerId {
    if (prefixArraysEquals(Ed25519_PREFIX, raw)) {
        return PeerId(raw.copyOfRange(Ed25519_PREFIX.size, raw.size))
    }
    throw IllegalStateException("Only Ed25519 expected")
}

private fun prefixArraysEquals(prefix: ByteArray, raw: ByteArray): Boolean {
    require(prefix.size < raw.size) { "Prefix not smaller" }
    for (i in prefix.indices) {
        if (prefix[i] != raw[i]) {
            return false
        }
    }
    return true
}

fun parsePeerId(raw: ByteArray): PeerId? {
    try {
        if (prefixArraysEquals(Ed25519_ID_PREFIX, raw)) {
            val peerId = PeerId(
                raw.copyOfRange(Ed25519_ID_PREFIX.size, raw.size)
            )
            return peerId
        }
    } catch (_: Throwable) {
    }
    return null
}


fun parseAddress(peerId: PeerId, bytes: ByteArray): Peeraddr? {

    val cis = Buffer()
    cis.write(bytes)

    var address: ByteArray? = null
    var port = 0.toUShort()
    try {

        while (!cis.exhausted()) {
            val code = readUnsignedVariant(cis)

            if (size(code) == 0) {
                continue
            }

            val part = readPart(code, cis) ?: return null
            if (part is ByteArray) {
                address = part
            }
            if (part is UShort) {
                port = part
            }
        }
    } catch (_: Throwable) { // should not occur
        return null
    }

    // check if address has a port, when it is not a dnsAddr
    if (port > 0.toUShort() && address != null) {
        return Peeraddr(peerId, address, port)
    }
    return null
}

fun parsePeeraddr(peerId: PeerId, raw: ByteArray): Peeraddr {
    return checkNotNull(parseAddress(peerId, raw)) { "Not supported peeraddr" }
}

fun multihash(peerId: PeerId): ByteArray {
    return concat(Ed25519_ID_PREFIX, peerId.hash)
}

internal fun concat(vararg chunks: ByteArray): ByteArray {
    var length = 0
    for (chunk in chunks) {
        check(length <= Int.MAX_VALUE - chunk.size) { "exceeded size limit" }
        length += chunk.size
    }
    val result = ByteArray(length)
    var pos = 0
    for (chunk in chunks) {
        chunk.copyInto(result, pos, 0, chunk.size)
        pos += chunk.size
    }
    return result
}

private val ALPHABET =
    "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz".toCharArray()
private val ENCODED_ZERO = ALPHABET[0]
private val INDEXES = createIndexes()

private fun createIndexes(): IntArray {
    val indexes = IntArray(128) { -1 }

    for (i in ALPHABET.indices) {
        indexes[ALPHABET[i].code] = i
    }
    return indexes
}

internal fun encode58(data: ByteArray): String {
    var input = data
    if (input.isEmpty()) {
        return ""
    }
    // Count leading zeros.
    var zeros = 0
    while (zeros < input.size && input[zeros].toInt() == 0) {
        ++zeros
    }
    // Convert base-256 digits to base-58 digits (plus conversion to ASCII characters)
    input = input.copyOf(input.size) // since we modify it in-place
    val encoded = CharArray(input.size * 2) // upper bound
    var outputStart = encoded.size
    var inputStart = zeros
    while (inputStart < input.size) {
        encoded[--outputStart] = ALPHABET[divmod(input, inputStart, 256, 58)
            .toInt()]
        if (input[inputStart].toInt() == 0) {
            ++inputStart // optimization - skip leading zeros
        }
    }
    // Preserve exactly as many leading encoded zeros in output as there were leading zeros in input.
    while (outputStart < encoded.size && encoded[outputStart] == ENCODED_ZERO) {
        ++outputStart
    }
    while (--zeros >= 0) {
        encoded[--outputStart] = ENCODED_ZERO
    }
    // Return encoded string (including encoded leading zeros).
    return encoded.concatToString(outputStart, encoded.size)
}

/**
 * Decodes the given base58 string into the original data bytes.
 *
 * @param input the base58-encoded string to decode
 * @return the decoded data bytes
 */
fun decode58(input: String): ByteArray {
    if (input.isEmpty()) {
        return byteArrayOf()
    }
    // Convert the base58-encoded ASCII chars to a base58 byte sequence (base58 digits).
    val input58 = ByteArray(input.length)
    for (i in input.indices) {
        val c = input[i]
        val digit = if (c.code < 128) INDEXES[c.code] else -1
        if (digit < 0) {
            throw Exception("InvalidCharacter in base 58")
        }
        input58[i] = digit.toByte()
    }
    // Count leading zeros.
    var zeros = 0
    while (zeros < input58.size && input58[zeros].toInt() == 0) {
        ++zeros
    }
    // Convert base-58 digits to base-256 digits.
    val decoded = ByteArray(input.length)
    var outputStart = decoded.size
    var inputStart = zeros
    while (inputStart < input58.size) {
        decoded[--outputStart] = divmod(input58, inputStart, 58, 256)
        if (input58[inputStart].toInt() == 0) {
            ++inputStart // optimization - skip leading zeros
        }
    }
    // Ignore extra leading zeroes that were added during the calculation.
    while (outputStart < decoded.size && decoded[outputStart].toInt() == 0) {
        ++outputStart
    }
    // Return decoded data (including original number of leading zeros).
    return decoded.copyOfRange(outputStart - zeros, decoded.size)
}

/**
 * Divides a number, represented as an array of bytes each containing a single digit
 * in the specified base, by the given divisor. The given number is modified in-place
 * to contain the quotient, and the return value is the remainder.
 *
 * @param number     the number to divide
 * @param firstDigit the index within the array of the first non-zero digit
 * (this is used for optimization by skipping the leading zeros)
 * @param base       the base in which the number's digits are represented (up to 256)
 * @param divisor    the number to divide by (up to 256)
 * @return the remainder of the division operation
 */
private fun divmod(number: ByteArray, firstDigit: Int, base: Int, divisor: Int): Byte {
    // this is just long division which accounts for the base of the input digits
    var remainder = 0
    for (i in firstDigit until number.size) {
        val digit = number[i].toInt() and 0xFF
        val temp = remainder * base + digit
        number[i] = (temp / divisor).toByte()
        remainder = temp % divisor
    }
    return remainder.toByte()
}
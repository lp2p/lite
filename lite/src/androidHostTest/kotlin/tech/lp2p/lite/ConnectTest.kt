package tech.lp2p.lite

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import tech.lp2p.lite.TestEnv.BOOTSTRAP
import tech.lp2p.lite.TestEnv.publicPeeraddrs
import tech.lp2p.lite.protos.connect
import tech.lp2p.lite.protos.identify
import tech.lp2p.lite.quic.Connection
import java.net.InetAddress
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.fail


class ConnectTest {
    @Test(expected = Exception::class) // can be Timeout or Connect Exception dependent of Network
    fun swarmPeer(): Unit = runBlocking(Dispatchers.IO) {
        val peerId = decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR")

        val host = "139.178.68.146"
        val peeraddr = createPeeraddr(peerId, InetAddress.getByName(host).address, 4001.toUShort())
        assertEquals(4001.toUShort(), peeraddr.port)

        assertEquals(peeraddr.peerId, peerId)

        // peeraddr is just a fiction (will fail)
        val lite = newLite()
        connect(lite, peeraddr)
        fail() // should not reached this point

    }


    @Test
    fun testIdentify(): Unit = runBlocking(Dispatchers.IO) {

        val server = newLite(
            bootstrap = BOOTSTRAP,
            reserve = { event: Any -> println("Reservation Event") }
        )


        val publicAddresses = publicPeeraddrs(server.peerId(), 5001)
        server.makeReservations(publicAddresses, 25, 120)


        val peeraddrs = server.reservations()
        for (addr in peeraddrs) {
            println("Reservation Address $addr")
        }
        println("Number of reservations " + server.numReservations())

        if (!server.hasReservations()) {
            println("nothing to test no dialable addresses")
            return@runBlocking
        }

        for (peeraddr in server.reservations()) {


            var connection: Connection
            try {
                connection = connect(server, peeraddr)
            } catch (throwable: Throwable) {
                println(
                    "Connection failed ${peeraddr.address()} " +
                            throwable.javaClass.simpleName
                )
                continue
            }

            try {
                assertNotNull(connection)
                val info = identify(connection)
                assertNotNull(info)
            } catch (throwable: Throwable) {
                error(throwable)
                fail()
            }

            assertNotNull(connection)
            connection.close()
        }
        server.shutdown()
    }

}

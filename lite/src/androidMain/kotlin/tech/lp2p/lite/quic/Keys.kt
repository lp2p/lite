package tech.lp2p.lite.quic

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.lite.tls.DecryptErrorAlert
import tech.lp2p.lite.tls.TlsState
import tech.lp2p.lite.tls.TrafficSecrets
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.AEADBadTagException
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec

data class Keys(
    val writerKey: ByteArray, val writeIV: ByteArray, val hp: ByteArray,
    val trafficSecret: ByteArray, val keyPhaseCounter: Int
) {
    val keyPhase: Short
        get() = (keyPhaseCounter % 2).toShort()

    /**
     * Check whether the key phase carried by a received packet still matches the current key phase; if not, compute
     * new keys (to be used for decryption). Note that the changed key phase can also be caused by packet corruption,
     * so it is not yet sure whether a key update is really in progress (this will be sure when decryption of the packet
     * failed or succeeded). This function will return true, when update is required.
     */
    fun checkKeyPhase(keyPhaseBit: Short): Boolean {
        return (keyPhaseCounter % 2) != keyPhaseBit.toInt()
    }

    fun aeadEncrypt(associatedData: ByteArray, nonce: ByteArray, input: ByteArray): ByteArray {
        try {
            // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.3:
            // "Prior to establishing a shared secret, packets are protected with AEAD_AES_128_GCM"
            val writeSpec = SecretKeySpec(writerKey, "AES")
            val aeadCipher = Cipher.getInstance("AES/GCM/NoPadding")
            // https://tools.ietf.org/html/rfc5116#section-5.3: "the tag length t is 16"
            val parameterSpec = GCMParameterSpec(128, nonce)
            aeadCipher.init(Cipher.ENCRYPT_MODE, writeSpec, parameterSpec)
            aeadCipher.updateAAD(associatedData)
            return aeadCipher.doFinal(input)
        } catch (throwable: Throwable) {
            throw IllegalStateException(throwable)
        }
    }

    fun aeadDecrypt(
        associatedData: ByteArray, nonce: ByteArray,
        input: ByteArray, inputOffset: Int, inputLength: Int
    ): ByteArray {
        if (input.size <= 16) {
            // https://www.rfc-editor.org/rfc/rfc9001.html#name-aead-usage
            // "These cipher suites have a 16-byte authentication tag and produce an output 16
            // bytes larger than their input."
            throw DecryptErrorAlert("ciphertext must be longer than 16 bytes")
        }
        try {
            // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.3:
            // "Prior to establishing a shared secret, packets are protected with AEAD_AES_128_GCM"
            val writeSpec = SecretKeySpec(writerKey, "AES")
            val aeadCipher = Cipher.getInstance("AES/GCM/NoPadding")
            // https://tools.ietf.org/html/rfc5116#section-5.3: "the tag length t is 16"
            val parameterSpec = GCMParameterSpec(128, nonce)
            aeadCipher.init(Cipher.DECRYPT_MODE, writeSpec, parameterSpec)
            aeadCipher.updateAAD(associatedData)
            return aeadCipher.doFinal(input, inputOffset, inputLength)
        } catch (decryptError: AEADBadTagException) {
            throw DecryptErrorAlert(decryptError.message)
        } catch (throwable: Throwable) {
            throw IllegalStateException(throwable)
        }
    }

    fun createHeaderProtectionMask(sample: ByteArray): ByteArray {
        val mask: ByteArray
        try {
            mask = getHeaderProtectionCipher(hp).doFinal(sample)
        } catch (e: IllegalBlockSizeException) {
            throw IllegalStateException(e)
        } catch (e: BadPaddingException) {
            throw IllegalStateException(e)
        }
        return mask
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Keys

        if (!writerKey.contentEquals(other.writerKey)) return false
        if (!writeIV.contentEquals(other.writeIV)) return false
        if (!hp.contentEquals(other.hp)) return false
        if (!trafficSecret.contentEquals(other.trafficSecret)) return false
        if (keyPhaseCounter != other.keyPhaseCounter) return false

        return true
    }

    override fun hashCode(): Int {
        var result = writerKey.contentHashCode()
        result = 31 * result + writeIV.contentHashCode()
        result = 31 * result + hp.contentHashCode()
        result = 31 * result + trafficSecret.contentHashCode()
        result = 31 * result + keyPhaseCounter
        return result
    }

    companion object {
        private const val KEY_LENGTH: Short = 16

        fun createInitialKeys(version: Int, initialSecret: ByteArray, client: Boolean): Keys {
            try {
                val initialNodeSecret = hkdfExpandLabel(
                    initialSecret,
                    if (client) "client in" else "server in",
                    32.toShort()
                )

                return computeKeys(version, initialNodeSecret)
            } catch (throwable: Throwable) {
                throw IllegalStateException(throwable)
            }
        }

        // See https://tools.ietf.org/html/rfc8446#section-7.1 for definition of HKDF-Expand-Label.
        private fun hkdfExpandLabel(
            secret: ByteArray,
            label: String,
            length: Short
        ): ByteArray {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1:
            // "The keys used for packet protection are computed from the TLS secrets using the KDF provided by TLS."
            val prefix =
                "tls13 ".toByteArray(Charsets.ISO_8859_1)

            val size = 2 + 1 + prefix.size +
                    label.toByteArray(Charsets.ISO_8859_1).size + 1 +
                    "".toByteArray(Charsets.ISO_8859_1).size
            val hkdfLabel = Buffer()
            hkdfLabel.writeShort(length)
            hkdfLabel.writeByte((prefix.size + label.toByteArray().size).toByte())
            hkdfLabel.write(prefix)
            hkdfLabel.write(label.toByteArray(Charsets.ISO_8859_1))
            hkdfLabel.writeByte(("".toByteArray(Charsets.ISO_8859_1).size).toByte())
            hkdfLabel.write("".toByteArray(Charsets.ISO_8859_1))

            require(hkdfLabel.size == size.toLong()) { "Invalid size" }
            return TlsState.expandHmac(secret, hkdfLabel.readByteArray(), length.toInt())
        }


        fun computeKeyUpdate(version: Int, keys: Keys): Keys {
            var prefix: String
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
            // "The current encryption level secret and the label "quic key" are
            //   input to the KDF to produce the AEAD key; the label "quic iv" is used
            //   to derive the IV, see Section 5.3.  The header protection key uses
            //   the "quic hp" label, see Section 5.4).  Using these labels provides
            //   key separation between QUIC and TLS, see Section 9.4."
            prefix = "quic "
            if (Version.isV2(version)) {
                // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-long-header-packet-types
                // "The labels used in [QUIC-TLS] to derive packet protection keys (Section 5.1), header protection keys (Section 5.4),
                //  Retry Integrity Tag keys (Section 5.8), and key updates (Section 6.1) change from "quic key" to "quicv2 key",
                //  from "quic iv" to "quicv2 iv", from "quic hp" to "quicv2 hp", and from "quic ku" to "quicv2 ku", to meet
                //  the guidance for new versions in Section 9.6 of that document."
                prefix = "quicv2 "
            }
            try {
                val trafficSecret =
                    hkdfExpandLabel(keys.trafficSecret, prefix + "ku", 32.toShort())

                val writeKey = hkdfExpandLabel(trafficSecret, prefix + "key", KEY_LENGTH)

                val writeIV = hkdfExpandLabel(trafficSecret, prefix + "iv", 12.toShort())

                return Keys(writeKey, writeIV, keys.hp, trafficSecret, (keys.keyPhaseCounter + 1))
            } catch (throwable: Throwable) {
                throw IllegalStateException(throwable)
            }
        }

        fun computeHandshakeKeys(version: Int, client: Boolean, secrets: TrafficSecrets): Keys {
            return if (client) {
                computeKeys(
                    version,
                    secrets.clientHandshakeTrafficSecret
                )
            } else {
                computeKeys(
                    version,
                    secrets.serverHandshakeTrafficSecret
                )
            }
        }

        fun computeApplicationKeys(version: Int, client: Boolean, secrets: TrafficSecrets): Keys {
            return if (client) {
                computeKeys(
                    version,
                    secrets.clientApplicationTrafficSecret
                )
            } else {
                computeKeys(
                    version,
                    secrets.serverApplicationTrafficSecret
                )
            }
        }


        private fun computeKeys(version: Int, trafficSecret: ByteArray): Keys {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
            // "The current encryption level secret and the label "quic key" are
            //   input to the KDF to produce the AEAD key; the label "quic iv" is used
            //   to derive the IV, see Section 5.3.  The header protection key uses
            //   the "quic hp" label, see Section 5.4).  Using these labels provides
            //   key separation between QUIC and TLS, see Section 9.4."


            var prefix = "quic "
            if (Version.isV2(version)) {
                // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-long-header-packet-types
                // "The labels used in [QUIC-TLS] to derive packet protection keys (Section 5.1), header protection keys (Section 5.4),
                //  Retry Integrity Tag keys (Section 5.8), and key updates (Section 6.1) change from "quic key" to "quicv2 key",
                //  from "quic iv" to "quicv2 iv", from "quic hp" to "quicv2 hp", and from "quic ku" to "quicv2 ku", to meet
                //  the guidance for new versions in Section 9.6 of that document."
                prefix = "quicv2 "
            }


            // https://tools.ietf.org/html/rfc8446#section-7.3
            val writeKey = hkdfExpandLabel(trafficSecret, prefix + "key", KEY_LENGTH)

            val writeIV = hkdfExpandLabel(trafficSecret, prefix + "iv", 12.toShort())

            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
            // "The header protection key uses the "quic hp" label"
            val hp = hkdfExpandLabel(trafficSecret, prefix + "hp", KEY_LENGTH)

            return Keys(writeKey, writeIV, hp, trafficSecret, 0)

        }

        private fun getHeaderProtectionCipher(hp: ByteArray): Cipher {
            try {
                // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.3
                // "AEAD_AES_128_GCM and AEAD_AES_128_CCM use 128-bit AES [AES] in electronic code-book
                // (ECB) mode."
                val hpCipher = Cipher.getInstance("AES/ECB/NoPadding")
                val keySpec = SecretKeySpec(hp, "AES")
                hpCipher.init(Cipher.ENCRYPT_MODE, keySpec)
                return hpCipher
            } catch (e: NoSuchAlgorithmException) {
                throw IllegalStateException(e)
            } catch (e: NoSuchPaddingException) {
                throw IllegalStateException(e)
            } catch (e: InvalidKeyException) {
                throw IllegalStateException(e)
            }
        }
    }
}

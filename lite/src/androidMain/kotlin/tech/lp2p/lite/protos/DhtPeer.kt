package tech.lp2p.lite.protos

import tech.lp2p.asen.Peeraddr
import java.math.BigInteger

data class DhtPeer(
    val peeraddr: Peeraddr,
    val replaceable: Boolean,
    val distance: BigInteger,
    val state: State
) :
    Comparable<DhtPeer> {
    fun done() {
        state.done = true
    }

    val isDone: Boolean
        get() = state.done

    val isStarted: Boolean
        get() = state.started

    fun start() {
        state.started = true
    }

    override fun compareTo(other: DhtPeer): Int {
        return distance.compareTo(other.distance)
    }

    class State {
        @Volatile
        var started: Boolean = false

        @Volatile
        var done: Boolean = false
    }
}

package tech.lp2p.lite.tls

import org.kotlincrypto.macs.hmac.sha2.HmacSHA256
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.PublicKey
import java.security.cert.X509Certificate
import java.security.spec.ECGenParameterSpec

abstract class TlsEngine :
    MessageProcessor, TrafficSecrets {
    var publicKey: PublicKey? = null
    var privateKey: PrivateKey? = null
    var state: TlsState? = null
    var remoteCertificate: X509Certificate? = null

    fun generateKeys(namedGroup: NamedGroup) {

        val keyPairGenerator: KeyPairGenerator
        if (namedGroup == NamedGroup.SECP256r1 || namedGroup == NamedGroup.SECP384r1 || namedGroup == NamedGroup.SECP521r1) {
            keyPairGenerator = KeyPairGenerator.getInstance("EC")
            keyPairGenerator.initialize(ECGenParameterSpec(namedGroup.name.lowercase()))
        } else {
            throw RuntimeException("unsupported group $namedGroup")
        }

        val keyPair = keyPairGenerator.genKeyPair()
        privateKey = keyPair.private
        publicKey = keyPair.public

    }

    // https://tools.ietf.org/html/rfc8446#section-4.4.4

    fun computeFinishedVerifyData(transcriptHash: ByteArray?, baseKey: ByteArray): ByteArray {
        val hashLength: Short = HASH_LENGTH
        val finishedKey = state!!.hkdfExpandLabel(baseKey, "finished", "", hashLength)

        val hmacAlgorithm = HmacSHA256(finishedKey)
        hmacAlgorithm.update(transcriptHash)
        return hmacAlgorithm.doFinal()

    }

    override val clientHandshakeTrafficSecret: ByteArray
        get() {
            if (state != null) {
                return state!!.clientHandshakeTrafficSecret
            } else {
                throw IllegalStateException("Traffic secret not yet available")
            }
        }

    override val serverHandshakeTrafficSecret: ByteArray
        get() {
            if (state != null) {
                return state!!.serverHandshakeTrafficSecret
            } else {
                throw IllegalStateException("Traffic secret not yet available")
            }
        }

    override val clientApplicationTrafficSecret: ByteArray
        get() {
            if (state != null) {
                return state!!.clientApplicationTrafficSecret
            } else {
                throw IllegalStateException("Traffic secret not yet available")
            }
        }

    override val serverApplicationTrafficSecret: ByteArray
        get() {
            if (state != null) {
                return state!!.serverApplicationTrafficSecret
            } else {
                throw IllegalStateException("Traffic secret not yet available")
            }
        }

}


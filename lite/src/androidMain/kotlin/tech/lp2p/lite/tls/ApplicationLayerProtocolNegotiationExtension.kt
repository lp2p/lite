/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

data class ApplicationLayerProtocolNegotiationExtension(val protocols: Array<String>) :
    Extension {
    override fun getBytes(): ByteArray {
        val protocolNamesLength = protocols.sumOf { p: String ->
            p.toByteArray(
                Charsets.UTF_8
            ).size
        }
        val size = 4 + 2 + protocols.size + protocolNamesLength
        val buffer = Buffer()

        buffer.writeShort(ExtensionType.APPLICATION_LAYER_PROTOCOL.value)
        buffer.writeShort((size - 4).toShort())
        buffer.writeShort((size - 6).toShort())
        for (protocol in protocols) {
            val protocolName = protocol.toByteArray(Charsets.UTF_8)
            buffer.writeByte(protocolName.size.toByte())
            buffer.write(protocolName)
        }
        require(buffer.size.toInt() == size)
        return buffer.readByteArray()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ApplicationLayerProtocolNegotiationExtension

        return protocols.contentEquals(other.protocols)
    }

    override fun hashCode(): Int {
        return protocols.contentHashCode()
    }

    companion object {
        fun create(protocol: String): ApplicationLayerProtocolNegotiationExtension {
            require(protocol.trim { it <= ' ' }.isNotEmpty()) { "protocol cannot be empty" }
            val protocols = arrayOf(protocol)
            return ApplicationLayerProtocolNegotiationExtension(protocols)
        }


        fun parse(
            buffer: Buffer,
            extensionLength: Int
        ): ApplicationLayerProtocolNegotiationExtension {
            val extensionDataLength = validateExtensionHeader(
                buffer,
                extensionLength,
                3
            )

            var protocolsLength = buffer.readShort().toInt()
            if (protocolsLength != extensionDataLength - 2) {
                throw DecodeErrorException("inconsistent lengths")
            }

            val protocols: MutableList<String> = ArrayList()
            while (protocolsLength > 0) {
                val protocolNameLength = buffer.readByte().toInt() and 0xff
                if (protocolNameLength > protocolsLength - 1) {
                    throw DecodeErrorException("incorrect length")
                }
                val protocolBytes = buffer.readByteArray(protocolNameLength)
                protocols.add(String(protocolBytes))
                protocolsLength -= (1 + protocolNameLength)
            }


            return ApplicationLayerProtocolNegotiationExtension(
                protocols.toTypedArray()
            )
        }
    }
}

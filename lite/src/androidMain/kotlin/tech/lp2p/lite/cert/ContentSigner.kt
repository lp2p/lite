package tech.lp2p.lite.cert

import java.security.Signature

/**
 * General interface for an operator that is able to create a signature from
 * a stream of output.
 */
interface ContentSigner {

    /**
     * Return the algorithm identifier describing the signature
     * algorithm and parameters this signer generates.
     *
     * @return algorithm oid and parameters.
     */
    fun getAlgorithmIdentifier(): AlgorithmIdentifier


    fun signature(): Signature

    /**
     * Returns a signature based on the current data written to the stream, since the
     * start or the last call to getSignature().
     *
     * @return bytes representing the signature.
     */
    fun getSignature(): ByteArray
}
